#!/usr/bin/env bash
  
ansible-galaxy install -r requirements.yml --force && \
  ansible-playbook bbb.yml -i hosts.yml -vv

