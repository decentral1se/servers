# Online Meeting Co-operative Servers



## bbb.webarch.org.uk

When setting up a Ubuntu 16.04 Xenial development server on Webarchitects
infrastructure, do the following on the Xen console to enable external SSH
and Ansible access first:

```bash
echo "nameserver 81.95.52.24" >> /etc/resolvconf/resolv.conf.d/head
echo "nameserver 81.95.52.30" >> /etc/resolvconf/resolv.conf.d/head
service resolvconf restart
apt update && apt dist-upgrade -y
apt install python3 python3-apt ssh-import-id -y
ssh-import-id chriscroome
```

